create database Consultorio_medicoo_MedCare_011;
use Consultorio_medicoo_MedCare_011; 
set sql_mode='';
create table usuario (
	id int not null auto_increment primary key,
	nombreusuario varchar(50),
	nombre varchar(50),
	apellido varchar(50),
	email varchar(255),
	password varchar(60),
	is_active boolean not null default 1,
	is_admin boolean not null default 0,
	created_at datetime
);

insert into usuario (nombreusuario,password,is_admin,is_active,created_at) value ("admin",sha1(md5("admin")),1,1,NOW());


create table paciente (
	id int not null auto_increment primary key,
	no varchar(50),
	nombre varchar(50),
	apellido varchar(50),
	genero varchar(1),
	dia_de_nac date,
	email varchar(255),
	direccion varchar(255),
	telefono varchar(255),
	image varchar(255),
	dolencia varchar(500),
	medicamentos varchar(500),
	alergia varchar(500),
	is_favorite boolean not null default 1,
	is_active boolean not null default 1,
	created_at datetime
);

create table categoria (
	id int not null auto_increment primary key,
	nombre varchar(200)
	);

insert into categoria (nombre) value ("Modulo 1");


create table medico (
	id int not null auto_increment primary key,
	no varchar(50),
	nombre varchar(50),
	apellido varchar(50),
	genero varchar(1),
	dia_de_nac date,
	email varchar(255),
	direccion varchar(255),
	telefono varchar(255),
	image varchar(255),
	is_active boolean not null default 1,
	created_at datetime,
	categoria_id int,
	foreign key (categoria_id) references categoria(id)
);



create table status (
	id int not null auto_increment primary key,
	nombre varchar(100)
);

insert into status (id,nombre) values (1,"Pendiente"), (2,"Aplicada"),(3,"No asistio"),(4,"Cancelada");

create table pago (
	id int not null auto_increment primary key,
	nombre varchar(100)
);

insert into pago (id,nombre) values  (1,"Pendiente"),(2,"Pagado"),(3,"Anulado");

create table reservacion(
	id int not null auto_increment primary key,
	titulo varchar(100),
	nota text,
	mensaje text,
	date_at varchar(50),
	time_at varchar(50),
	created_at datetime,
	paciente_id int,
	sintomas text,
	dolencia text,
	medicamentos text,
	usuario_id int,
	medico_id int,
	precio double,
	is_web boolean not null default 0,
	pago_id int not null default 1,
	foreign key (pago_id) references payment(id),
	status_id int not null default 1,
	foreign key (status_id) references status(id),
	foreign key (usuario_id) references usuario(id),
	foreign key (paciente_id) references paciente(id),
	foreign key (medico_id) references medico(id)
);