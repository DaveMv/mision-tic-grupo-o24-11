package co.edu.medcare.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.edu.medcare.models.entity.Paciente;
import co.edu.medcare.repository.PacienteRepository;


@Component
public class PacienteServiceImpl implements PacienteService{

	
	@Autowired
	public PacienteRepository pacienteRepository;
	
	@Override
	public List<Paciente> findAll() {
		// TODO Auto-generated method stub
		return pacienteRepository.findAll();
	}

	@Override
	public Optional<Paciente> findById(Integer id) {
		// TODO Auto-generated method stub
		return pacienteRepository.findById(id);
	}

	@Override
	public Paciente save(Paciente paciente) {
		// TODO Auto-generated method stub
		return pacienteRepository.save(paciente);
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		pacienteRepository.deleteById(id);
	}

	@Override
	public List<Paciente> pacienteNombres(String Nombres) {
		// TODO Auto-generated method stub
		return pacienteRepository.pacienteNombres(Nombres);
	}

}
