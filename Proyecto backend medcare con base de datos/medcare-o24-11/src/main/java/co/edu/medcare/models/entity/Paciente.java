package co.edu.medcare.models.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="paciente")
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name ="id_paciente")
	private Integer ID_Paciente;
	
	@Column (name ="nombres")
	private String Nombres;
	
	@Column (name ="apellidos")
	private String Apellidos;
	
	@Column (name ="peso")
	private Float Peso;
	
	@Column (name ="altura")
	private Float Altura;
	
	@Column (name ="edad")
	private Integer Edad;
	
	@Column (name ="telefono")
	private String Telefono;
	
	@Column (name ="alergias")
	private String Alergias;
	
	@Column (name ="enfermedades")
	private String Enfermedades;
	
	@Column (name ="tipo_sangre")
	private String Tipo_Sangre;
		

	public Paciente() {
		super();
	}

	public Paciente(Integer id_Paciente, String nombres, String apellidos, Float peso, Float altura, Integer edad,
			String telefono, String alergias, String enfermedades, String tipo_Sangre) {
		super();
		this.ID_Paciente = id_Paciente;
		this.Nombres = nombres;
		this.Apellidos = apellidos;
		this.Peso = peso;
		this.Altura = altura;
		this.Edad = edad;
		this.Telefono = telefono;
		this.Alergias = alergias;
		this.Enfermedades = enfermedades;
		this.Tipo_Sangre = tipo_Sangre;
	}

	public Integer getID_Paciente() {
		return ID_Paciente;
	}

	public void setID_Paciente(Integer id_Paciente) {
		ID_Paciente = id_Paciente;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public Float getPeso() {
		return Peso;
	}

	public void setPeso(Float peso) {
		Peso = peso;
	}

	public Float getAltura() {
		return Altura;
	}

	public void setAltura(Float altura) {
		Altura = altura;
	}

	public Integer getEdad() {
		return Edad;
	}

	public void setEdad(Integer edad) {
		Edad = edad;
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		Telefono = telefono;
	}

	public String getAlergias() {
		return Alergias;
	}

	public void setAlergias(String alergias) {
		Alergias = alergias;
	}

	public String getEnfermedades() {
		return Enfermedades;
	}

	public void setEnfermedades(String enfermedades) {
		Enfermedades = enfermedades;
	}

	public String getTipo_Sangre() {
		return Tipo_Sangre;
	}

	public void setTipo_Sangre(String tipo_Sangre) {
		Tipo_Sangre = tipo_Sangre;
	}

	@Override
	public String toString() {
		return "Paciente [ID_Paciente=" + ID_Paciente + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos + ", Peso="
				+ Peso + ", Altura=" + Altura + ", Edad=" + Edad + ", Telefono=" + Telefono + ", Alergias=" + Alergias
				+ ", Enfermedades=" + Enfermedades + ", Tipo_Sangre=" + Tipo_Sangre + "]";
	}


	
	
}
