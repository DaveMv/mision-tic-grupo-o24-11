package co.edu.medcare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedcareO2411Application {

	public static void main(String[] args) {
		SpringApplication.run(MedcareO2411Application.class, args);
	}

}
