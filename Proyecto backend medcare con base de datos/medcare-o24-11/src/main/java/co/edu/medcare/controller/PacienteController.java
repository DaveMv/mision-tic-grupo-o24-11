package co.edu.medcare.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.medcare.models.entity.Paciente;
import co.edu.medcare.models.service.PacienteService;


@SuppressWarnings("unused")
@RequestMapping("/api/paciente")
@RestController
//@CrossOrigin(origins="")
public class PacienteController {

@Autowired
PacienteService pacienteService;

@GetMapping("/listar")
public List<Paciente> listar(){
	return pacienteService.findAll();
}

@GetMapping("/{id}")
public Optional<Paciente> findById(@PathVariable Integer id){
	return pacienteService.findById(id);
}

@PostMapping
public Paciente save(@RequestBody Paciente paciente) {
	return pacienteService.save(paciente);
}

@PostMapping("/edit/{id}")
public Paciente update(@RequestBody Paciente paciente, @PathVariable Integer id) {
	Paciente pacienteBD = pacienteService.findById(id).get();
	pacienteBD.setNombres(paciente.getNombres());
	pacienteBD.setApellidos(paciente.getApellidos());
	pacienteBD.setPeso(paciente.getPeso());
	pacienteBD.setAltura(paciente.getAltura());
	pacienteBD.setEdad(paciente.getEdad());
	pacienteBD.setTelefono(paciente.getTelefono());
	pacienteBD.setAlergias(paciente.getAlergias());
	pacienteBD.setEnfermedades(paciente.getEnfermedades());
	pacienteBD.setTipo_Sangre(paciente.getTipo_Sangre());
	return pacienteService.save(pacienteBD);
}

//metodo personalizado
@GetMapping("/nombres/{Nombres}")
public List<Paciente> pacienteNombres(@PathVariable String Nombres){
	return pacienteService.pacienteNombres(Nombres);
}

@DeleteMapping("/{id}")
public void deleteById(@PathVariable Integer id) {
	pacienteService.deleteById(id);
}

}
