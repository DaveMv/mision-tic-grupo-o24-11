Create database ClinicaMedCare;

Use ClinicaMedCare;

Create Table Especialidad (
    ID_Especialidad int unsigned not null auto_increment unique primary key,
    Nombre nvarchar(50) not null,
    Descripcion nvarchar(150) not null,
    Estado boolean default true not null
);

Create Table Medico (
    ID_Medico int unsigned not null auto_increment unique primary key,
    Nombres nvarchar(100) not null,
    Apellidos nvarchar(100) not null,
 /*   Hora_Inicial nvarchar(15) not null,
    Hora_Final nvarchar(15) not null,*/
    ID_Especialidad int unsigned not null,
    Foreign Key (ID_Especialidad)references Especialidad (ID_Especialidad),
  /*  L boolean default false not null,
    M boolean default false not null,
    X boolean default false not null,
    J boolean default false not null,
    V boolean default false not null,
    S boolean default false not null,
    D boolean default false not null,*/
    ID_Usuario int unsigned default 0 not null,
    Estado boolean default true not null
);

Create Table Paciente (
    ID_Paciente int unsigned not null auto_increment unique primary key,
    Nombres nvarchar(100) not null,
    Apellidos nvarchar(100) not null,
    Peso float not null,
    Altura float not null,
    Edad int not null,
    Telefono nvarchar(9) not null,
    Alergias nvarchar(200) not null,
    Enfermedades nvarchar(200) not null,
    Tipo_Sangre nvarchar(5) not null,
    Estado boolean default true not null,
	created_at datetime
);

Create Table Cita (
    ID_Cita int unsigned not null auto_increment unique primary key,
    Fecha_Cita date not null,
    Hora_Cita nvarchar(15) not null,
    Dia_Cita nvarchar(15) not null,
    Semana_Cita int not null,
    ID_Medico int unsigned not null,
    Foreign Key (ID_Medico)
        references Medico (ID_Medico),
    ID_Paciente int unsigned not null,
    Foreign Key (ID_Paciente)
        references Paciente (ID_Paciente),
    Estado nvarchar(15) not null
);

Create Table Servicio (
    ID_Servicio int unsigned not null auto_increment unique primary key,
    Nombre_Servicio nvarchar(50) not null,
    Descripcion_Servicio nvarchar(150) not null,
    Precio_Servicio decimal(20 , 2 ) not null,
    Estado boolean default true not null
);

Create Table Usuario (
    ID_Usuario int unsigned not null auto_increment unique primary key,
    Nombre_Usuario nvarchar(50) not null,
    Contrasena_Usuario nvarchar(100) not null,
    Rol_Usuario nvarchar(20) not null,
    Estado boolean default true not null,
	created_at datetime
);

Create Table Consulta (
    ID_Consulta int unsigned not null auto_increment unique primary key,
    ID_Cita int unsigned not null,
    Foreign Key (ID_Cita) references Cita (ID_Cita),
    Descripcion_Consulta nvarchar(200) not null,
    Diagnostico nvarchar(200) not null,
    Receta nvarchar(500) not null,
    Estado boolean default true not null,
	created_at datetime
);

Insert into Usuario (Nombre_Usuario, Contrasena_Usuario, Rol_Usuario)
values ('Admin', '1234', 'Admin');

Create Table Pago (
    ID_Pago int unsigned not null auto_increment unique primary key,
    Fecha_Pago timestamp default current_timestamp not null,
    ID_Paciente int unsigned not null,
    Foreign Key (ID_Paciente)
        references Paciente (ID_Paciente),
    ID_Usuario int unsigned not null,
    Foreign Key (ID_Usuario)
        references Usuario (ID_Usuario),
    Estado boolean default true not null
);

Create Table Detalle_Pago (
    ID_Detalle_Pago int unsigned not null auto_increment unique primary key,
    ID_Pago int unsigned not null,
    Foreign Key (ID_Pago) references Pago (ID_Pago),
    ID_Servicio int unsigned not null,
    Foreign Key (ID_Servicio)references Servicio (ID_Servicio),
    Cantidad int unsigned not null,
    Estado boolean default true not null
);


Create Table Dia_Medico(
	ID_Dia_Medico int unsigned not null auto_increment unique primary key,
	ID_Medico int unsigned not null,
    Foreign Key (ID_Medico) references Medico (ID_Medico),
	Dia nvarchar(1) not null,
	Estado boolean default true not null
);

Create Table Hora_Medico(
	ID_Hora_Medico int unsigned not null auto_increment unique primary key,
	ID_Dia_Medico int unsigned not null,
    Foreign Key (ID_Dia_Medico) references Dia_Medico (ID_Dia_Medico),
	Hora_Inicial nvarchar(15) not null,
    Hora_Final nvarchar(15) not null,
	Estado boolean default true not null
);
