function loadData(){
    let request = sendRequest('Pacientes/list', 'GET', '')
    let table = document.getElementById('pacientes-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.ID_Paciente}</th>
                    <td>${element.Nombres}</td>
                    <td>${element.Apellidos}</td>
                    <td>${element.Peso}</td>
                    <th>${element.Altura}</th>
                    <td>${element.Edad}</td>
                    <td>${element.Telefono}</td>
                    <td>${element.Alergias}</td>
                    <th>${element.Enfermedades}</th>
                    <td>${element.Tipo_Sangre}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "paciente_formedit.html?id=${element.ID_Paciente}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deletePaciente(${element.ID_Paciente})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadPaciente(ID_Paciente){
    let request = sendRequest('pacientes/'+ID_Paciente, 'GET', '')
    let id = document.getElementById('paciente-id')
    let nombres = document.getElementById('pac-nombre')
    let apellidos = document.getElementById('pac-apellido')
    let peso = document.getElementById('pac-peso')
    let altura = document.getElementById('pac-altura')
    let edad = document.getElementById('pac-edad')
    let telefono = document.getElementById('pac-telefono')
    let alergias = document.getElementById('pac-alergias')
    let enfermedades = document.getElementById('pac-enfermedades')
    let tipo_sangre = document.getElementById('pac-tiposangre')
    request.onload = function(){
        
        let data = request.response
        id.value = data.ID_Paciente
        nombres.value = data.Nombres
        apellidos.value = data.Apellidos
        peso.value = data.Peso
        altura.value = data.Altura
        edad.value = data.Edad
        telefono.value = data.Telefono
        alergias.value = data.Alergias
        enfermedades.value = data.Enfermedades
        tipo_sangre.value = data.Tipo_Sangre
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deletePaciente(id){
    let request = sendRequest('medicamentos/'+id, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function savePaciente(){
    let id = document.getElementById('paciente-id').value
    let nombres = document.getElementById('pac-nombre').value
    let apellidos = document.getElementById('pac-apellido').value
    let peso = document.getElementById('pac-peso').value
    let altura = document.getElementById('pac-altura').value
    let edad = document.getElementById('pac-edad').value
    let telefono = document.getElementById('pac-telefono').value
    let alergias = document.getElementById('pac-alergias').value
    let enfermedades = document.getElementById('pac-enfermedades').value
    let tipo_sangre = document.getElementById('pac-tiposangre').value
    var data = {'ID_Paciente': id,'Nombres':nombres,'Apellidos': apellidos, 'Peso': peso,'Altura': altura,'Edad': edad,'Telefono': telefono,'Alergias': alergias,'Enfermedades': enfermedades,'Tipo_Sangre': tipo_sangre}
    let request = sendRequest('pacientes/', 'POST', data)
    request.onload = function(){
        window.location = 'pacientes.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}
