const url="https://minticloud.uis.edu.co/c3s24grupo11/"
//const url = "http://localhost:8080/"

 function sendRequest(endpoint,method,data){
    let request = new XMLHttpRequest();
    request.open(method,url+endpoint);
    request.setRequestHeader('Content-Type','application/json');
    request.responseType = 'json';
    request.send(data?JSON.stringify(data):data);
    return request
} 
