package co.edu.medcare.models.service;

import java.util.List;
import java.util.Optional;
import co.edu.medcare.models.service.PacienteService;
import co.edu.medcare.models.entity.Paciente;

public interface PacienteService {

	public List<Paciente> findAll();
	public Optional<Paciente> findById(Integer id);
	public Paciente save(Paciente paciente);
	public void deleteById(Integer id);
	public List<Paciente> pacienteNombres(String Nombres);
}
