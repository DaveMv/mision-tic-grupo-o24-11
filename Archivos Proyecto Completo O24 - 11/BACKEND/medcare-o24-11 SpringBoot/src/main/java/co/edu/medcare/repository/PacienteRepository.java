package co.edu.medcare.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.edu.medcare.models.entity.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Integer>{

	@Query("select c from Paciente c where c.Nombres =?1")
	public List<Paciente> pacienteNombres(String Nombres);
}
