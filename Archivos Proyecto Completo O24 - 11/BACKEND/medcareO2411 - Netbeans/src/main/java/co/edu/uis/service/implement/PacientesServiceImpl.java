package co.edu.uis.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import co.edu.uis.models.Pacientes;
import co.edu.uis.repository.PacientesRepository;
import co.edu.uis.service.PacientesService;

@Service
public class PacientesServiceImpl implements PacientesService {
	
	@Autowired
	private PacientesRepository pacientesRepository;
	
	@Override
	@Transactional(readOnly=false)
	public Pacientes save(Pacientes pacientes) {
		return pacientesRepository.save(pacientes);
	}
	
	@Override
	@Transactional(readOnly=false)
	public void delete(Integer id) {
		pacientesRepository.deleteById(id);
	}
	
	@Override
	@Transactional(readOnly=true)
	public Pacientes findById(Integer id) {
		return pacientesRepository.findById(id).orElse(null);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<Pacientes> findAll() {
		return (List<Pacientes>) pacientesRepository.findAll();
	}
}

