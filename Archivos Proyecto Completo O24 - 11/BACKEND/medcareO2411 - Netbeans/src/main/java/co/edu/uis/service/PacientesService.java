package co.edu.uis.service;

import java.util.List;

import co.edu.uis.models.Pacientes;

public interface PacientesService {
	
	public Pacientes save(Pacientes pacientes);
	public void delete(Integer id);
	public Pacientes findById(Integer id);
	public List<Pacientes> findAll();

}
