package co.edu.uis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Medcare02411Application {

	public static void main(String[] args) {
		SpringApplication.run(Medcare02411Application.class, args);
	}

}
