package co.edu.uis.repository;

import org.springframework.data.repository.CrudRepository;

import co.edu.uis.models.Pacientes;

public interface PacientesRepository extends CrudRepository<Pacientes, Integer>{

}
