package co.edu.uis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.uis.models.Pacientes;
import co.edu.uis.service.PacientesService;

@RestController
@CrossOrigin("mysql://localhost:3306")
@RequestMapping("/pacientes")
public class PacientesController {
	
@Autowired
private PacientesService pacientesservice;

@PostMapping(value="/")
public ResponseEntity<Pacientes> agregar(@RequestBody Pacientes pacientes){
	Pacientes obj= pacientesservice.save(pacientes);
	return new ResponseEntity<>(obj, HttpStatus.OK);
}

@DeleteMapping(value="/{id}")
public ResponseEntity<Pacientes> eliminar(@PathVariable Integer id){
	Pacientes obj=pacientesservice.findById(id);
	if(obj!=null)
		pacientesservice.delete(id);
	else
		return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
	return new ResponseEntity<>(obj,HttpStatus.OK);
	}

@PutMapping(value="/")
public ResponseEntity<Pacientes> editar(@RequestBody Pacientes pacientes){
Pacientes obj= pacientesservice.findById(pacientes.getID_Paciente
        ());
if(obj!=null)
{
	obj.setID_Paciente(pacientes.getID_Paciente());
	obj.setNombres(pacientes.getNombres());
	obj.setApellidos(pacientes.getApellidos());
	obj.setPeso(pacientes.getPeso());
        obj.setAltura(pacientes.getAltura());
        obj.setEdad(pacientes.getEdad());
        obj.setTelefono(pacientes.getTelefono());
        obj.setAlergias(pacientes.getAlergias());
        obj.setEnfermedades(pacientes.getEnfermedades());
        obj.setTipo_Sangre(pacientes.getTipo_Sangre());
        
	pacientesservice.save(obj);
}
else
	return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

@GetMapping(value="/list")
public List<Pacientes> consultarTodo(){
	return pacientesservice.findAll();
}

@GetMapping(value="/List/{id}")
public Pacientes consultarPorId(@PathVariable Integer id) {
	return pacientesservice.findById(id);
}

}
