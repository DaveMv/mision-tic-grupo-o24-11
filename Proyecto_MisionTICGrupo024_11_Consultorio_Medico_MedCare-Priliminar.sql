create database Consultorio_Medico_MedCare; 
use Consultorio_Medico_MedCare;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

#Base de datos: `Consultorio_Medico_MedCare`
#Estructuras de las bases de datos preliminar, sujeto a cambios**************

#Estructura de tabla para la tabla `categoria_gastos`
CREATE TABLE `categoria_gastos` (
  `id_categoria` int(200) NOT NULL,
  `descripcion` varchar(200) NOT NULL
);


#Volcado de datos para la tabla `categoria_gastos`
#datos de prueba
INSERT INTO `categoria_gastos` (`id_categoria`, `descripcion`) VALUES
(1, 'mas tabletas');

describe table cita;

#Estructura de tabla para la tabla `categoria_gastos_farmacia`


CREATE TABLE `categoria_gastos_farmacia` (
  `id_categoria` int(200) NOT NULL,
  `descripcion` varchar(200) NOT NULL
);


#Volcado de datos para la tabla `categoria_gastos_farmacia`
#datos de prueba
INSERT INTO `categoria_gastos_farmacia` (`id_categoria`, `descripcion`) VALUES
(4, 'guantes quirurgicos'),
(5, 'mascarillas');


#Estructura de tabla para la tabla `cita`


CREATE TABLE `cita` (
  `id_cita` int(200) NOT NULL,
  `id_paciente` int(200) NOT NULL,
  `id_medico` int(200) NOT NULL,
  `fecha` date NOT NULL,
  `estado_cita` varchar(200) NOT NULL,
  `observaciones` varchar(200) NOT NULL,
  `horario` varchar(200) NOT NULL
);


#Volcado de datos para la tabla `cita`
#datos de prueba
INSERT INTO `cita` (`id_cita`, `id_paciente`, `id_medico`, `fecha`, `estado_cita`, `observaciones`, `horario`) VALUES
(1, 20, 17, '2020-08-09', 'reservado', 'venir a la hora', '20'),
(2, 9, 12, '2020-08-18', 'reservado', 'hora exacrta', '9'),
(3, 20, 17, '2020-08-18', 'reservado', 'rapdio', '15');


#Estructura de tabla para la tabla `detalles_pedido`

CREATE TABLE `detalles_pedido` (
  `id_detalles` int(200) NOT NULL,
  `id_pedido` int(200) NOT NULL,
  `id_producto` int(200) NOT NULL,
  `cantidad` varchar(200) NOT NULL,
  `id_cliente` int(200) NOT NULL
);


#Estructura de tabla para la tabla `detalles_pedido_medicina`


CREATE TABLE `detalles_pedido_medicina` (
  `id_detalles` int(200) NOT NULL,
  `id_pedido` int(200) NOT NULL,
  `id_producto` int(200) NOT NULL,
  `cantidad` int(200) NOT NULL,
  `id_cliente` int(200) NOT NULL
);


#Volcado de datos para la tabla `detalles_pedido_medicina`
#datos de prueba
INSERT INTO `detalles_pedido_medicina` (`id_detalles`, `id_pedido`, `id_producto`, `cantidad`, `id_cliente`) VALUES
(3, 3, 4, 2, 20),
(4, 4, 5, 1, 20),
(5, 5, 5, 2, 20);


#Estructura de tabla para la tabla `detalle_preescripcion`

CREATE TABLE `detalle_preescripcion` (
  `id_detalle_preescripcion` int(200) NOT NULL,
  `medicina` varchar(200) NOT NULL,
  `dosis` varchar(200) NOT NULL,
  `frecuencia` varchar(200) NOT NULL,
  `dias` int(200) NOT NULL,
  `instruccion` varchar(1000) NOT NULL,
  `id_preescripcion` int(200) NOT NULL
);


#Volcado de datos para la tabla `detalle_preescripcion`
#datos de prueba
INSERT INTO `detalle_preescripcion` (`id_detalle_preescripcion`, `medicina`, `dosis`, `frecuencia`, `dias`, `instruccion`, `id_preescripcion`) VALUES
(1, 'panadol', '50', '2 veces al dia', 3, 'despues de cada comida', 2);


#Estructura de tabla para la tabla `empresa`

CREATE TABLE `empresa` (
  `id_empresa` int(100) NOT NULL,
  `empresa` varchar(200) NOT NULL,
  `ruc` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `descripcion` varchar(2000) NOT NULL,
  `imagen` varchar(2000) NOT NULL,
  `correo` varchar(200) NOT NULL,
  `simbolo_moneda` varchar(200) NOT NULL,
  `tipo_moneda` varchar(200) NOT NULL
);

#Volcado de datos para la tabla `empresa`
#datos de prueba
INSERT INTO `empresa` (`id_empresa`, `empresa`, `ruc`, `direccion`, `telefono`, `descripcion`, `imagen`, `correo`, `simbolo_moneda`, `tipo_moneda`) VALUES
(1, 'clinica sistemasenoferta', '4324', 'av san marino', '242432334', 'empresa clinica sistemasenoferta', '79325289_107343034108000_8331319381153808384_n.jpg', 'sistemasenoferta@gmail.com', '$./', 'nuevo sol');


#Estructura de tabla para la tabla `gastos`

CREATE TABLE `gastos` (
  `id_gastos` int(200) NOT NULL,
  `cantidad` float NOT NULL,
  `nota` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `id_categoria` int(200) NOT NULL
);


#Estructura de tabla para la tabla `gastos_farmacia`

CREATE TABLE `gastos_farmacia` (
  `id_gastos` int(200) NOT NULL,
  `cantidad` int(200) NOT NULL,
  `nota` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `id_categoria` int(200) NOT NULL
);


#Volcado de datos para la tabla `gastos_farmacia`
#datos de prueba
INSERT INTO `gastos_farmacia` (`id_gastos`, `cantidad`, `nota`, `fecha`, `id_categoria`) VALUES
(1, 3, 'solo queda poco', '2020-08-08', 5);

#Estructura de tabla para la tabla `history_log`

CREATE TABLE `history_log` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date` datetime NOT NULL
);

# Volcado de datos para la tabla `history_log`
#datos de prueba
INSERT INTO `history_log` (`log_id`, `user_id`, `action`, `date`) VALUES
(1, 1, 'has logged in the system at ', '2018-11-27 07:58:26');



#Estructura de tabla para la tabla `horario`

CREATE TABLE `horario` (
  `id_horario` int(200) NOT NULL,
  `nombre_horario` varchar(200) NOT NULL
);

#Volcado de datos para la tabla `horario`
#datos de prueba
INSERT INTO `horario` (`id_horario`, `nombre_horario`) VALUES
(1, '8:30 AM a 08:45 AM');

#Estructura de tabla para la tabla `horario_medico`

CREATE TABLE `horario_medico` (
  `id_horario_medico` int(200) NOT NULL,
  `dia_laborable` varchar(200) NOT NULL,
  `hora_inicio` varchar(200) NOT NULL,
  `hora_fin` varchar(200) NOT NULL,
  `cita_duracion` varchar(200) NOT NULL,
  `id_medico` int(200) NOT NULL
);

#Volcado de datos para la tabla `horario_medico`
#datos de prueba
INSERT INTO `horario_medico` (`id_horario_medico`, `dia_laborable`, `hora_inicio`, `hora_fin`, `cita_duracion`, `id_medico`) VALUES
(2, 'lunes', '8 am', '11 am', '20 minutos', 17);


#Estructura de tabla para la tabla `pedidos`


CREATE TABLE `pedidos` (
  `id_pedido` int(200) NOT NULL,
  `fecha` date NOT NULL,
  `id_sesion` int(100) NOT NULL,
  `id_cliente` int(200) NOT NULL,
  `tipo_venta` varchar(200) NOT NULL,
  `monto_pagado` float NOT NULL,
  `total` float NOT NULL,
  `id_medico` int(200) NOT NULL
);

#Volcado de datos para la tabla `pedidos`
#datos de prueba

INSERT INTO `pedidos` (`id_pedido`, `fecha`, `id_sesion`, `id_cliente`, `tipo_venta`, `monto_pagado`, `total`, `id_medico`) VALUES
(4, '2020-08-09', 5, 20, 'Contado', 0, 0, 17);

#Estructura de tabla para la tabla `pedido_medicina`

CREATE TABLE `pedido_medicina` (
  `id_pedido` int(200) NOT NULL,
  `fecha` date NOT NULL,
  `id_sesion` int(200) NOT NULL,
  `id_cliente` int(200) NOT NULL,
  `tipo_venta` varchar(200) NOT NULL,
  `monto_pagado` float NOT NULL,
  `total` float NOT NULL,
  `id_medico` int(200) NOT NULL
);


# Volcado de datos para la tabla `pedido_medicina`
#datos de prueba

INSERT INTO `pedido_medicina` (`id_pedido`, `fecha`, `id_sesion`, `id_cliente`, `tipo_venta`, `monto_pagado`, `total`, `id_medico`) VALUES
(3, '2020-08-09', 5, 20, 'Contado', 10, 10, 8);


#Estructura de tabla para la tabla `preescripcion`

CREATE TABLE `preescripcion` (
  `id_preescripcion` int(200) NOT NULL,
  `id_cliente` int(200) NOT NULL,
  `id_medico` int(200) NOT NULL,
  `historia` varchar(1000) NOT NULL,
  `id_sesion` int(200) NOT NULL,
  `fecha` date NOT NULL
);


# Volcado de datos para la tabla `preescripcion`
#datos de prueba
INSERT INTO `preescripcion` (`id_preescripcion`, `id_cliente`, `id_medico`, `historia`, `id_sesion`, `fecha`) VALUES
(1, 20, 17, 'gripe', 17, '2022-08-09');



#Estructura de tabla para la tabla `procedimiento_pago`

CREATE TABLE `procedimiento_pago` (
  `id_procedimiento_pago` int(200) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `precio_venta` float NOT NULL,
  `estado` varchar(200) NOT NULL
);

# Volcado de datos para la tabla `procedimiento_pago`
#datos de prueba

INSERT INTO `procedimiento_pago` (`id_procedimiento_pago`, `nombre`, `descripcion`, `precio_venta`, `estado`) VALUES
(1, 'ecografia', 'realizada on exito', 34, 'a');


#Estructura de tabla para la tabla `producto`

CREATE TABLE `producto` (
  `id_pro` int(100) NOT NULL,
  `nombre_pro` varchar(100) NOT NULL,
  `descripcion` varchar(2000) NOT NULL,
  `unidad` varchar(100) NOT NULL,
  `precio_venta` float NOT NULL,
  `precio_compra` float NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `stock` varchar(200) NOT NULL,
  `estado` varchar(200) NOT NULL
);

# Volcado de datos para la tabla `producto`
#datos de prueba
INSERT INTO `producto` (`id_pro`, `nombre_pro`, `descripcion`, `unidad`, `precio_venta`, `precio_compra`, `imagen`, `stock`, `estado`) VALUES
(4, 'agua oxigenada', 'para limpiar heridas', 'botellas', 5, 4, '', '50', 'a');


#Estructura de tabla para la tabla `usuario`


CREATE TABLE `usuario` (
  `id` int(200) NOT NULL,
  `usuario` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(200) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL
);

#Volcado de datos para la tabla `usuario`

INSERT INTO `usuario` (`id`, `usuario`, `password`, `imagen`, `tipo`, `nombre`, `apellido`, `telefono`, `correo`) VALUES
(1, 'admin', 'a1Bz20ydqelm8m1wql21232f297a57a5a743894a0e4a801fc3', '', 'administrador', 'sistemas', 'en oferta', '54345', 'administrador@gmail.com'),
(2, 'siba', 'a1Bz20ydqelm8m1wql21232f297a57a5a743894a0e4a801fc3', '', 'recepcionista', 'siba', 'siba', '2342423', 'receocionista@gmail.com'),
(3, 'paciente', 'a1Bz20ydqelm8m1wqld243800a7d0ba0f87081bcdd832bb05f', '', 'paciente', 'rodrigo', 'llanos', '98798798', 'paciente@gmail.com');


#Estructura de tabla para la tabla `vacaciones`


CREATE TABLE `vacaciones` (
  `id_vacaciones` int(200) NOT NULL,
  `fecha` date NOT NULL,
  `id_medico` int(200) NOT NULL
);

#Volcado de datos para la tabla `vacaciones`
#datos de prueba

INSERT INTO `vacaciones` (`id_vacaciones`, `fecha`, `id_medico`) VALUES
(1, '2020-08-21', 1);



#Índices para tablas volcadas
#Indice de cada tabla con datos correspondientes a tablas principales
#1. Indices de la tabla `categoria_gastos`

ALTER TABLE `categoria_gastos`
  ADD PRIMARY KEY (`id_categoria`);

#2. Indices de la tabla `categoria_gastos_farmacia`

ALTER TABLE `categoria_gastos_farmacia`
  ADD PRIMARY KEY (`id_categoria`);

#3. Indices de la tabla `cita`

ALTER TABLE `cita`
  ADD PRIMARY KEY (`id_cita`);

#4. Indices de la tabla `detalles_pedido`

ALTER TABLE `detalles_pedido`
  ADD PRIMARY KEY (`id_detalles`);


#5. Indices de la tabla `detalles_pedido_medicina`

ALTER TABLE `detalles_pedido_medicina`
  ADD PRIMARY KEY (`id_detalles`);

#6 Indices de la tabla `detalle_preescripcion`

ALTER TABLE `detalle_preescripcion`
  ADD PRIMARY KEY (`id_detalle_preescripcion`);

#7.  Indices de la tabla `empresa`

ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

#8. Indices de la tabla `gastos`

ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id_gastos`);

#9. Indices de la tabla `gastos_farmacia`

ALTER TABLE `gastos_farmacia`
  ADD PRIMARY KEY (`id_gastos`);

#10. Indices de la tabla `history_log`

ALTER TABLE `history_log`
  ADD PRIMARY KEY (`log_id`);

#11. Indices de la tabla `horario`

ALTER TABLE `horario`
  ADD PRIMARY KEY (`id_horario`);


#12. Indices de la tabla `horario_medico`

ALTER TABLE `horario_medico`
  ADD PRIMARY KEY (`id_horario_medico`);

#12. Indices de la tabla `pedidos`

ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`);

#13. Indices de la tabla `pedido_medicina`

ALTER TABLE `pedido_medicina`
  ADD PRIMARY KEY (`id_pedido`);

#14. Indices de la tabla `preescripcion`

ALTER TABLE `preescripcion`
  ADD PRIMARY KEY (`id_preescripcion`);

#15. Indices de la tabla `procedimiento_pago`

ALTER TABLE `procedimiento_pago`
  ADD PRIMARY KEY (`id_procedimiento_pago`);

#16. Indices de la tabla `producto`

ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_pro`);

#17. Indices de la tabla `usuario`

ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

#18. Indices de la tabla `vacaciones`

ALTER TABLE `vacaciones`
  ADD PRIMARY KEY (`id_vacaciones`);

#AUTO_INCREMENT de las tablas volcadas
# AUTO_INCREMENT de la tabla `categoria_gastos`

ALTER TABLE `categoria_gastos`
  MODIFY `id_categoria` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

# AUTO_INCREMENT de la tabla `categoria_gastos_farmacia`

ALTER TABLE `categoria_gastos_farmacia`
  MODIFY `id_categoria` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

# AUTO_INCREMENT de la tabla `cita`

ALTER TABLE `cita`
  MODIFY `id_cita` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

# AUTO_INCREMENT de la tabla `detalles_pedido`

ALTER TABLE `detalles_pedido`
  MODIFY `id_detalles` int(200) NOT NULL AUTO_INCREMENT;

# AUTO_INCREMENT de la tabla `detalles_pedido_medicina`

ALTER TABLE `detalles_pedido_medicina`
  MODIFY `id_detalles` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

# AUTO_INCREMENT de la tabla `detalle_preescripcion`

ALTER TABLE `detalle_preescripcion`
  MODIFY `id_detalle_preescripcion` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

#AUTO_INCREMENT de la tabla `empresa`

ALTER TABLE `empresa`
  MODIFY `id_empresa` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

# AUTO_INCREMENT de la tabla `gastos`

ALTER TABLE `gastos`
  MODIFY `id_gastos` int(200) NOT NULL AUTO_INCREMENT;

#AUTO_INCREMENT de la tabla `gastos_farmacia`

ALTER TABLE `gastos_farmacia`
  MODIFY `id_gastos` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

#AUTO_INCREMENT de la tabla `history_log`

ALTER TABLE `history_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;

# AUTO_INCREMENT de la tabla `horario`

ALTER TABLE `horario`
  MODIFY `id_horario` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

# AUTO_INCREMENT de la tabla `horario_medico`

ALTER TABLE `horario_medico`
  MODIFY `id_horario_medico` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

# AUTO_INCREMENT de la tabla `pedidos`

ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

#AUTO_INCREMENT de la tabla `pedido_medicina`

ALTER TABLE `pedido_medicina`
  MODIFY `id_pedido` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

# AUTO_INCREMENT de la tabla `preescripcion`

ALTER TABLE `preescripcion`
  MODIFY `id_preescripcion` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

#AUTO_INCREMENT de la tabla `procedimiento_pago`

ALTER TABLE `procedimiento_pago`
  MODIFY `id_procedimiento_pago` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

#AUTO_INCREMENT de la tabla `producto`

ALTER TABLE `producto`
  MODIFY `id_pro` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

# AUTO_INCREMENT de la tabla `usuario`

ALTER TABLE `usuario`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

# AUTO_INCREMENT de la tabla `vacaciones`

ALTER TABLE `vacaciones`
  MODIFY `id_vacaciones` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

